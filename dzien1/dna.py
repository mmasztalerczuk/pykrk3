def przetestuj(test):
    # test = [('G', 'T'), ('C', 'C'), ('T', 'G')]
    distance = 0

    for pair in test:
        # ('G', 'T')
        if pair[0] != pair[1]:
            distance += 1
    return distance

f1 = input("Podaj dna 1 ojca")
f2 = input("Podaj dna 2 ojca")

dna_child = input("Podaj dna dziecka")
test_1 = zip(f1, dna_child)
test_2 = zip(f2, dna_child)
dist1 = przetestuj(test_1)
dist2 = przetestuj(test_2)

if dist1 > dist2:
    print("Ojcem jest pan numer 2")
else:
    print("Ojce jest pan numer 1")


a = 1
b = 2

c = a
a = b
b = c

a, b = b, a