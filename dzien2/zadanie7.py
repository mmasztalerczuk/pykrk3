# 1. Napisz funkcje ktora dla zadanej listy sprawdza czy sa w niej duplikaty


def check_if_any_duplicate(l):
    return len(l) != len(set(l))

l = [1,2,3,4]
b = [1,2,3,4,4]
print(check_if_any_duplicate(l))
print(check_if_any_duplicate(b))