# 1. Wczytaj liczbe n
# 2. Wczyta n ciagu znakow
# 3. Wypisz elemety listy od tylu
# 4. Wypisz 3 wyraz
# 5. Wypisz 4 pierwsze elementy
# 6. Wypisz 3 ostatnie elementy

def wczytaj():
    n = int(input())
    l = []
    for i in range(n):
        l.append(input())
    return l

moja_lista = wczytaj()

print(moja_lista[::-1])
print(moja_lista[2])
print(moja_lista[:3])
print(moja_lista[:-4:-1])
print(moja_lista[len(moja_lista)-3:])