# 1. Wygeneruj liczby od 0 do n
# 2. Wypisz wszystkie liczby, ale:
#        -> Jezeli liczba jest podzielna przez
#           7 nie wypisuj jej
#        -> Jezeli liczba jest rowna 13^2 zakoncz
#           dzialanie programu

# n = int(input())
#
# for i in range(n):
#     if i%7 == 0:
#         continue
#     elif i == 13**2:
#          break
#     else:
#         pass
#     print(i)

def foo():
    print("foo")
def bar():
    print("bar")

foo()
bar()
foo()
